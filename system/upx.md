upx compresses binaries, so they take up less disk space, but take longer to start.

Make a binary, such as ls, compressed:

> upx ls


