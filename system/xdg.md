
What filetype is this file?

> xdg-mime query filetype aif.pdf

`application/pdf`

Make `application/pdf` open with zathura as default:

> xdg-mime default org.pwmt.zathura.desktop application/pdf

