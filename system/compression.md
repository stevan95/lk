# Tar

## Basics

* --create -c

* --list -t

* --extract --get -x

* --file=file -f file




Look in tar file:

> tar -tf filename.tar.gz

Look in detail:

> tar -tvf filename.tar.gz

# Zip

Zip file:

> zip name.zip file

Zip directory:

> zip -r name.zip dir

Update existing entries.

> zip -ru name.zip dir file1 file2

Delete file from the zipfile.

> zip -d name.zip dfile

# 7z for .img.xz

> 7z x file.img.xz

