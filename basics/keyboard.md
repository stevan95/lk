# Set Layout

Set layout to British English.

> setxkbmap -layout gb

| Language | short |
|:--------|:------|
| Polish  | pl |
| Serbian | rs |

Set 'alt + shift', as the command which cycles through the British English, Polish and Serbian keyboard layout.

> setxkbmap -layout gb,pl,rs -option grp:alt_shift_toggle

