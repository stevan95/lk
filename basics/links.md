Link from X to Y.

> ln -s X ../otherdir/Y

Links cause ownership headaches.  Solve this with -h:

> chown -h user1 mysymlink

