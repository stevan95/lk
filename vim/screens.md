
Make a horizontal split with:

> :sp

> C-w s

Or a vertical split with:

> :vsp
> C-w v
