Search and replace the first 'one' found with 'two':

> :%s/one/two/

Same, but replace 'one' globally:

> :%s/one/two/g

