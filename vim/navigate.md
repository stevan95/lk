
| Move | Command      |
|:-----|:-------------|
|Down page | C-f      |
| Down half page | C-d |
| Up page | C-b |
| Up half page | C-u |

## Scroll

> C-e

> C-y

## Jumps

Go through your last jumps:

> C-I

> C-O

Go to the last and previous places you've changed:

> g;

> g,

# Project Structure

Make a 20 character 'visual split' in the current working directory ('`.`').

> :20vs .

Change the view for this:

> C-w x


