# Install New Dictionaries

If the path doesn't exist then:

> sudo mkdir -p /usr/share/stardict/dic

Then move the dictionaries there.

# Words

To look up 'cat' in all languages, just do:

> sdcv cat

Look at dictionaries you have with:

> sdcv -l

To use a specific dictionary, like `en_rs`, do:

> sdcv -u en_rs

