# Basic Commands

> H = highest part
> L = lowest part
> gg = top

> g$ = most right.
> g0 = most left.

> \ = insert middle
> > = insert left
> < = insert right

gb4 = to to cell b4

> x = delete a cell
> aa = see all text in cells

> f = format cells so you can see it.
> fl = format wider right
> fh = format smaller left

> fj = decrease decimal value
> fk = increase decimal value

# Edit

> e = edit a number
> E = edit text
> dc = delete column
> yc = yank column
> dr = delete row
> p = literal paste
> Pc = paste mutatis mutandis

#Functions

> =@sum(A1:A4) = add A1 to A4
> =@avg(B1:B4) = average B1 to B4
> =@max(B1:B4) = maximum of those numbers
> =@min(B1:B8) = minimumof those numbers
> =@prod(C1:C8) = multiply C1 to C8

