# Sendmail

Compose a message like this:

```
To: alice@domain.com
Subject: my new mail server
From: literally@anything.com

This is the message

```

... and put it in ~/mail.txt.
Then send with sendmail using:

> sendmail ~/mail.txt


