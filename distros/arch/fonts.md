# Basics

Update font-cache:

> fc-cache

List fonts:

> fc-list

Grab the part of the font name you need for Xresources:

> fc-list | cut -d: -f2

Add field 3 for styles.
