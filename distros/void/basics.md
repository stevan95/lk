
# vkpurge

Old Void kernels are left on the boot partition.  List them with:

> vkpurge list

Remove one with:

> vkpurge 2.8.2_4

Remove all but the latest with:

> vkpurge rm all


# Brightness
/sys/class/backlight/*/brightness


