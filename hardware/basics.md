# Motherboard Information

> sudo dmidecode

Motherboard info, upgrading BIOS, memory capacity, LAN connections.

# Disks

View currently inserted disks:

lsblk

# CPU

> cat /proc/cpuinfo

