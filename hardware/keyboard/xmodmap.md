Ensure you're not stuck in CAPS on mode:

> xmodmap -e 'clear Lock' 

Remap the keyboard so CAPS LOCK=ESC:

> xmodmap -e 'keycode 0x42=Escape' 

