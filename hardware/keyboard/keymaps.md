Find easy-to-read keymapping lists in `/usr/share/X11/xkb/keycodes/symbols/pc`.

If this doesn't work, try keymaps.

Keymaps codes can be found in `/usr/share/X11/xkb/keycodes/evdev`.
Swap the numbers to swap symbols.
